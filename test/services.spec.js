describe('Services', function() {
  beforeEach(module('services'));

  var $httpBackend;
  var dataService;

  beforeEach(
    inject(function(_dataService_, _$httpBackend_){
        $httpBackend = _$httpBackend_;
        dataService = _dataService_;
    }));

  describe('get data', function() {
    beforeEach(function() {
      $httpBackend.expectGET('/data.json')
      .respond(200, {data: [{},{},{},{},{},{}]});

      dataService.get();  
      $httpBackend.flush();
    });

    it('should send an HTTP GET request', function() {
      $httpBackend.verifyNoOutstandingExpectation();
      $httpBackend.verifyNoOutstandingRequest();
    });
  });
});