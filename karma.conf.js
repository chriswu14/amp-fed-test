
module.exports = function(config) {
    config.set({

        basePath: '',
        frameworks: ['mocha', 'chai'],
        files: [
            'build/lib/angular.min.js',
            'node_modules/angular-mocks/angular-mocks.js',
            'build/app.js',
            'test/*.spec.js'
        ],

        // list of files to exclude
        exclude: [],
        preprocessors: {},
        reporters: ['progress'],
        port: 9876,
        colors: true,
        logLevel: config.LOG_INFO,
        browsers: ['Chrome'],
        autoWatch : false,
        singleRun: true
    });
};
