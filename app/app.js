angular.module('services',[])
.service('dataService', function($http){
  this.get = function(){
    return $http.get('/data.json');
  }
})

angular.module('app', ['services'])
.controller('appCtrl', ['$scope', 'dataService', function($scope, dataService){
  
  init();
  
  function init(){
    dataService.get().then(function(res){
      $scope.peopleList = res.data.data;      
    });
  }

}]);