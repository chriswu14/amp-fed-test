module.exports = function(grunt) {

    grunt.initConfig({

        pkg: grunt.file.readJSON('package.json'),
        sassFiles: 'app/**/*.scss',

        connect: {
            server: {
                options: {
                    base: 'build'
                }
            }
          },

        sass: {
            dist: {
                files: {
                    'build/css/site.css': 'app/site.scss'
                }
            }
        },

         copy: {
           root: {
                files: [{
                    expand: true,
                    cwd: './',
                    src: ['data.json', 'index.html'],
                    dest: 'build/'
                }]
            },
            app: {
                files: [{
                    expand: true,
                    cwd: 'app/',
                    src: ['*.js', '**/*.js'],
                    dest: 'build/'
                }]
            },
            img: {
                files: [{
                    expand: true,
                    cwd: 'img/',
                    src: ['*'],
                    dest: 'build/img/'
                }]
            },
            lib: {
                files: [{
                    expand: true,
                    cwd: 'bower_components/angular/',
                    src: ['*.min.js'],
                    dest: 'build/lib/'
                }]
            }
        },

        watch: {
            sass: {
                tasks: ['sass'],
                files: 'app/**/*.scss'
            },

        }
    });

    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-sass');
    grunt.loadNpmTasks('grunt-contrib-connect');
    grunt.loadNpmTasks('grunt-contrib-watch');


    grunt.registerTask('dev', ['default', 'watch']);
    grunt.registerTask('default', ['sass', 'copy', 'connect']);
};